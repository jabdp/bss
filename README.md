# BSS--客户订单管理系统

#### 介绍
基于jabdp的客户订单管理系统案例

#### 软件架构
软件架构说明


#### 安装教程

### 1.下载平台
首先是下载jabdp平台，[下载地址](http://jabdp.7yxx.com/html/download.html)
### 2.平台应用部署
把下载下来的文件解压，注意：文件目录和名字要全英文的，而且不要有空格
### 3.创建和连接数据库（以MYSQL为例）
项目开发前首先需要定义数据来源，实际用户系统最常见的就是数据保存在数据库中，并且在不断更新中。jabdc文件夹了已经自带了mysql数据库服务，而且mysql是绿色免安装的。不过mysql数据库的客户端工具需要开发者自行下载，我们推荐使用Navicat工具。

3.1、打开自己下载好的Navicat客户端工具，新建连接，连接到jabdp自带的mysql服务。默认的端口号是3366，用户名是root,密码是jabdp。

Jabdp集成的mysql服务已经自带了一个叫jabdp的数据库，默认的工程项目就是使用这个的。

3.2、也可以新建一个自己的数据库，就在建好的连接下创建一个空的数据库。
### 4.运行sql文件
将下载下来的sql文件（simpleBSS.sql）在navicat上自己创建的数据库中运行，便可将本项目的基础数据导入到自己的数据库中。
### 5.修改数据库链接配置文件
修改iPlatform工程目录WEB-INF\classes\application.properties文件：
4.1、修改jdbc.url、jdbc.username、jdbc.password修改为新建数据库的相关属性；一般只要修改数据库名就可以了。

```
jdbc.url=jdbc:mysql://127.0.0.1:3366/填写自己创建的数据库名(比如:jabdp)?useUnicode=true&characterEncoding=utf-8
```

```
jdbc.username=root
```

```
jdbc.password=jabdp
```
5.2、删除 iPlatform\upload目录下的所有文件。
5.3、执行iPlatform工程目录WEB-INF\init-db.bat文件，即可进行数据库初始化（包括建表及数据初始化）。
### 6.启用JABDP应用服务
6.1、点击启动服务.bat，右键以管理员身份运行，默认会自动注册mysql服务（第一次运行）；
6.2、如果命令窗口关闭，则需要再次点击启动服务.bat，右键以管理员身份运行，启动tomcat服务；
6.3、启动成功后，打开chrome浏览器，输入http://127.0.0.1:9090/iDesigner（设计器）和http://127.0.0.1:9090/iPlatform（平台，用于项目运行）分别浏览即可。
### 7.导入账套
将下载下来的账套文件（simpleBSS.jwp），导入到设计器中。
### 8.更新账套
将导入的账套保存后，全量更新到平台端。具体的更新操作，请查看线上文档：[使用手册](http://jabdp.7yxx.com/doc/)。[注：请查看第二章快速入门的新建账套部分]
### 9.问题解疑
如有问题，可以加QQ群提问。群号：801507856

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
